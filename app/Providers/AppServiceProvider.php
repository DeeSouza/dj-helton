<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\Galeria;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if(Schema::hasTable('galerias')){
            $galeria = Galeria::get();
            view()->share('sliders', $galeria);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
