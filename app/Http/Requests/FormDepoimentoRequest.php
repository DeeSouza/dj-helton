<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormDepoimentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'          => 'required',
            'depoimento'    => 'required',
            'foto'          => 'dimensions:width=500,height=500|image:jpg,png'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required'         => 'O campo <code>:attribute</code> é obrigatório.',
            'depoimento.required'   => 'O campo <code>:attribute</code> é obrigatório.',
            'foto.dimensions'       => 'A <code>:attribute</code> precisa ter <strong>:widthpx</strong> de largura e <strong>:heightpx</strong> de altura.',
            'foto.image'            => 'A <code>:attribute</code> precisa ser <strong>jpg</strong> ou <strong>png</strong>.'
        ];
    }
}
