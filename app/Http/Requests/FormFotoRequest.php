<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormFotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fotos' => 'between:1,3'
        ];
    }

    public function messages()
    {
        return [
            'fotos.between' => 'Você só pode carregar 3 fotos por vez.'
        ];
    }
}
