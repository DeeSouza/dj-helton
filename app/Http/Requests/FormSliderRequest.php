<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'        => 'required',
            'foto'          => 'dimensions:width=1920,height=923,|image:jpg,png'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'titulo.required'       => 'O campo <code>:attribute</code> é obrigatório.',
            'foto.dimensions'       => 'A <code>:attribute</code> precisa ter <strong>:widthpx</strong> de largura e <strong>:heightpx</strong> de altura.',
            'foto.image'            => 'A <code>:attribute</code> precisa ser <strong>jpg</strong> ou <strong>png</strong>.'
        ];
    }
}
