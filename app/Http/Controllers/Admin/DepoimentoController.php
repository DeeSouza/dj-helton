<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\FormDepoimentoRequest;
use App\Http\Controllers\Controller;
use App\Depoimento;
use File;

class DepoimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depoimentos = Depoimento::orderBy('created_at', 'desc')->get();
        return view('backend.panel.depoimentos.list')->with('depoimentos', $depoimentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.panel.depoimentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormDepoimentoRequest $request)
    {
        $fileName = null;

        if($request->has('foto')){
            $fileExtension  = $request->file('foto')->getClientOriginalExtension();
            $fileName       = str_slug($request->get('nome')).'_'.date('H_m_s').'.'.$fileExtension;
            $request->file('foto')->move(public_path('img/depoimentos'), $fileName);
        }
        
        $create                 = new Depoimento;
        $create->nome           = $request->get('nome');
        $create->cargo          = $request->get('cargo');
        $create->sexo           = $request->get('sexo');
        $create->depoimento     = $request->get('depoimento');
        $create->foto           = $fileName;
        $create->save();

        return redirect('admin/depoimentos')->with('alert-success', 'Depoimento Criado com Sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $depoimento = Depoimento::findOrFail($id);
        return view('backend.panel.depoimentos.edit')->with('depoimento', $depoimento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormDepoimentoRequest $request, $id)
    {
        $create = Depoimento::find($id);

        if($request->has('foto')){
            $fileExtension  = $request->file('foto')->getClientOriginalExtension();
            $fileName       = str_slug($request->get('nome')).'_'.date('H_m_s').'.'.$fileExtension;
            $request->file('foto')->move(public_path('img/depoimentos'), $fileName);
            $create->foto   = $fileName;
        }
        
        $create->nome           = $request->get('nome');
        $create->sexo           = $request->get('sexo');
        $create->cargo          = $request->get('cargo');
        $create->depoimento     = $request->get('depoimento');
        $create->save();

        return redirect('admin/depoimentos')->with('alert-success', 'Depoimento Atualizado com Sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $create = Depoimento::find($id);
        File::delete(public_path('img/depoimentos/'.$create->foto));
        $create->delete();

        return [
            'status' => true
        ];
    }
}
