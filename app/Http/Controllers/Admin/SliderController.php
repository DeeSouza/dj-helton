<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\FormSliderRequest;
use App\Http\Controllers\Controller;
use App\Galeria;

class SliderController extends Controller
{
    function index(){
    	$galeria = Galeria::get();
    	return view('backend.panel.slider.list')->with('sliders', $galeria);
    }

    function edit($id){
        $galeria = Galeria::find($id);
        return view('backend.panel.slider.edit')->with('galeria', $galeria);
    }

    function update(FormSliderRequest $request, $id){
    	$create = Galeria::find($id);

    	if($request->has('foto')){
    	    $fileExtension  	= $request->file('foto')->getClientOriginalExtension();
    	    $fileName       	= 'helton-borges-eventos-slider-'.uniqid().'.'.$fileExtension;
    	    $request->file('foto')->move(public_path('img/sliders'), $fileName);
    	    $create->updated_at = date('Y-m-d H:i:s');
    	    $create->foto   	= $fileName;
    	}

        $create->titulo = $request->get('titulo');
    	
    	$create->save();

    	return redirect('admin/slider')->with('alert-success', 'Slider Atualizado com Sucesso.');
    }
}
