<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\FormFotoRequest;
use App\Http\Controllers\Controller;
use App\Foto;
use Image;
use File;

class FotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = Foto::orderBy('created_at', 'DESC')->get();

        return view('backend.panel.fotos.list')->with('fotos', $fotos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormFotoRequest $request)
    {
        ini_set("memory_limit", "512M");
        ini_set("upload_max_size", "50M");
        ini_set("post_max_size", "50M");

        $files = $request->file('fotos');

        foreach ($files as $key => $value) {
            $fileName       = 'helton-borges-eventos-'.uniqid();
            $fileExtension  = $value->getClientOriginalExtension();
            $fileName       = $fileName.'.'.$fileExtension;
            $value->move(public_path('img/projetos/bigger'), $fileName);

            // Thumb
            $img = Image::make(public_path('img/projetos/bigger/'.$fileName));
            $img->resize(350, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img->resizeCanvas(350, 200, 'center', false, '#C7C7C7');
            $img->save(public_path('img/projetos/thumb/'.$fileName));

            // Grande
            $img = Image::make(public_path('img/projetos/bigger/'.$fileName));
            $img->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img->resizeCanvas(1280, null, 'center', false, '#C7C7C7');
            $img->save(public_path('img/projetos/bigger/'.$fileName));

            $newFoto            = new Foto;
            $newFoto->nome      = '';
            $newFoto->foto      = $fileName;
            $newFoto->descricao = '';
            $newFoto->save();
        }
        
        return redirect('admin/fotos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foto = Foto::find($id);

        return response()->json($foto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $foto               = Foto::find($id);
        $foto->nome         = $request->get('nome');
        $foto->descricao    = $request->get('descricao');
        $foto->save();

        return response([
            'status' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteFotoBulk(Request $request){
        $fotos = $request->get('ids');
        $fotos = json_decode($fotos);

        $lines = Foto::whereIn('id', $fotos);

        foreach($lines->get() as $item){
            File::delete('img/projetos/thumb/'.$item->foto);
            File::delete('img/projetos/bigger/'.$item->foto);
        }

        $lines->delete();

        return response([
            'status' => true 
        ]);
    }
}
