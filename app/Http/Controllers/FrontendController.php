<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Depoimento;
use App\Foto;
use App\Galeria;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index(){
        $depoimentos    = Depoimento::orderBy('created_at', 'DESC')->get();
        $fotos          = Foto::orderBy('created_at', 'DESC')->get();

    	return view('frontend.home')->with('depoimentos', $depoimentos)->with('fotos', $fotos);
    }

    public function send(Request $request){
        try{
            Mail::to('helton@heltonborgeseventos.com.br')->send(new ContactMail($request->except('_token')));
        } catch(\Exception $e){
            $message    = '<div class="response-send">
                            <div>
                                <img src="'.url('img/icon-error.svg').'" alt="ERROR" />
                                <p>Houve um problema ao enviar a mensagem. Tente novamente mais tarde.</p>
                            </div>
                        </div>';

            return $response   = [
                'message'   => $message
            ];
        }

    	$message 	= '<div class="response-send">
							<div>
								<img src="'.url('img/icon-ok.svg').'" alt="OK" />
								<p>Mensagem enviada com sucesso. Aguarde nosso contato em breve, por favor.</p>
							</div>
						</div>';

    	$response 	= [
    		'message' 	=> $message
    	];

    	return response()->json($response);
    }
}
