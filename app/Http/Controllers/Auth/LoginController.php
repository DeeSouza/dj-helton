<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function index(){
        return view('backend.auth.login');
    }

    public function login(Request $request){
    	$login = Auth::attempt([
    		'username' => $request->get('username'),
    		'password' => $request->get('password')
    	]);

    	if($login){
    		return redirect()->intended('admin/panel');
    	}
    	else{
    		return redirect('auth')->with('Credenciais Inválidas');;
    	}
    }

    public function logout(){
    	Auth::logout();

    	return redirect('auth');
    }
}
