<?php

	// Página Principal
	Route::post('send-mail', 'FrontendController@send');
	Route::get('/', 'FrontendController@index');

	Route::prefix('admin')->namespace('Admin')->middleware('auth.site')->group(function(){
		Route::get('panel', 'PanelController@index');
		Route::resource('depoimentos', 'DepoimentoController');
		Route::resource('fotos', 'FotoController');
		Route::resource('slider', 'SliderController');
		Route::post('fotos/delete-foto-bulk', 'FotoController@deleteFotoBulk');
	});

	Route::prefix('auth')->namespace('Auth')->group(function(){
		Route::get('logout', 'LoginController@logout');

		Route::middleware('guest')->group(function(){
			Route::post('login', 'LoginController@login');
			Route::get('/', 'LoginController@index');
		});
	});

	Route::get('admin', function(){
		return redirect('auth');
	});