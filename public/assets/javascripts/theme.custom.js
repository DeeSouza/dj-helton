/* Add here all your JS customizations */
var urlBase;

$(document).ready(function(){
	urlBase = $('body').data('base');

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	if($('a.delete-depoimento')[0]){
		$('a.delete-depoimento').on('click', function(){
			var id = $(this).attr('id').replace('depoimento-', '');	

			$('#iddepoimento').val(id);
		});

		$('.confirm-delete-depoimento').on('click', function(){
			var id = $('#iddepoimento').val();
			$.ajax({
				type: "DELETE",
				url: urlBase + '/admin/depoimentos/' + id,
				success: function(response){
					if(response){
						$('#row-' + id).fadeOut(300);
					}
				}
			})
		});
	}

	$('#do-upload').on('click', function(){
		$('#fotos').click();
	});

	$('input[type=file]').change(function () {
	    var fileCount = this.files.length;
	    $('#count_files').text('Você tem ' + fileCount + ' para carregar.');
	});

	$('#deleteFotos').on('click', function(){
		var fotos 		= $('input[name=fotos-projetos]:checked');
		var ids   		= [];
		var promises 	= [];

		for(var i = 0; i < fotos.length; i++){
			ids.push($(fotos[i]).attr('id').replace('file-', ''));
		}

		$.ajax({
			url: urlBase + '/admin/fotos/delete-foto-bulk',
			type: "POST",
			data: {
				ids: JSON.stringify(ids)
			},
			success: function(response){
				console.log(fotos);
				fotos.closest('.o-foto').fadeOut(300);
			}
		})
	});

	$('.edit-foto').on('click', function(){
		var id 		= $(this).attr('id').replace('foto-', '');
		var form 	= $('#form-edit');
		var action 	= form.attr('action');

		form.attr('action', action + '/' + id);
		$('#idfoto').val(id);

		$.ajax({
			url: urlBase + '/admin/fotos/' + id,
			type: "GET",
			beforeSend: function(){
				console.log('Carregando...');
			},
			success: function(response){
				form.find('input[name=nome]').val(response.nome);
				form.find('textarea[name=descricao]').val(response.descricao);
			}
		});
	});

	$('#saveDetail').on('click', function(){
		var formdata = $('#form-edit').serialize();
		
		$.ajax({
			url: urlBase + '/admin/fotos/' + $('#idfoto').val(),
			type: "PUT",
			data: formdata,
			beforeSend: function(){
				console.log('Carregando...');
			},
			success: function(response){
				console.log('Saved');
				$.magnificPopup.close();

				new PNotify({
					title: 'Pronto!',
					text: 'Projeto atualizado com sucesso!',
					type: 'success'
				});
			}
		})
	})
});
