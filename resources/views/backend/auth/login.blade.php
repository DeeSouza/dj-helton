<!doctype html>
<html class="fixed">
	<head>
		<title>Sistema Administrativo do Site - DJ Helton</title>

		<!-- Basic -->
		<meta charset="UTF-8">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		{!! Html::style("assets/vendor/bootstrap/css/bootstrap.css") !!}
		{!! Html::style("assets/vendor/font-awesome/css/font-awesome.css") !!}
		{!! Html::style("assets/vendor/magnific-popup/magnific-popup.css") !!}
		{!! Html::style("assets/vendor/bootstrap-datepicker/css/datepicker3.css") !!}

		<!-- Theme CSS -->
		{!! Html::style("assets/stylesheets/theme.css") !!}

		<!-- Skin CSS -->
		{!! Html::style("assets/stylesheets/skins/default.css") !!}

		<!-- Theme Custom CSS -->
		{!! Html::style("assets/stylesheets/theme-custom.css") !!}

		<!-- Head Libs -->
		{!! Html::script("assets/vendor/modernizr/modernizr.js") !!}

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="{!! URL::to('auth') !!}" class="logo pull-left">
					{!! Html::image("img/helton-borges-eventos-logo-text.png", "DJ", ["height" => 20]) !!}
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Acessar</h2>
					</div>
					<div class="panel-body">
						{!! Form::open(['id' => 'form-login', 'url' => 'auth/login', 'method' => 'POST', 'autocomplete' => 'off']) !!}
							<div class="form-group mb-lg">
								<label>Identificação de Usuário</label>
								<div class="input-group input-group-icon">
									<input name="username" type="text" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Senha</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Entrar</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright {!! date('Y') !!}. Todos os direitos reservados. 
				<a href="http://www.limonadabrasil.com.br" target="_blank">Limonada Brasil</a>.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		{!! Html::script("assets/vendor/jquery/jquery.js") !!}
		{!! Html::script("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") !!}
		{!! Html::script("assets/vendor/bootstrap/js/bootstrap.js") !!}
		{!! Html::script("assets/vendor/nanoscroller/nanoscroller.js") !!}
		{!! Html::script("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js") !!}
		{!! Html::script("assets/vendor/magnific-popup/magnific-popup.js") !!}
		{!! Html::script("assets/vendor/jquery-placeholder/jquery.placeholder.js") !!}
		
		<!-- Theme Base, Components and Settings -->
		{!! Html::script("assets/javascripts/theme.js") !!}
		
		<!-- Theme Custom -->
		{!! Html::script("assets/javascripts/theme.custom.js") !!}
		
		<!-- Theme Initialization Files -->
		{!! Html::script("assets/javascripts/theme.init.js") !!}

	</body>
</html>