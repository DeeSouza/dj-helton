@extends('layouts.backend')

@section('title-page', 'Fotos do Slider')

@section('contents')
	<div class="row">
		<div class="col-md-12">
			@if(session()->has('alert-success'))
				<div class="alert alert-success">{!! session()->get('alert-success') !!}</div>
			@endif

			@if($sliders->count() > 0)
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
						</div>

						<h2 class="panel-title">Lista de Fotos</h2>
					</header>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover mb-none table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Nome</th>
										<th>Opções</th>
									</tr>
								</thead>
								<tbody>
									@foreach($sliders as $item)
										<tr id="row-{!! $item->id !!}">
											<td>{!! $item->id !!}</td>
											<td>{!! $item->titulo !!}</td>
											<td>
												<a href="{!! url('admin/slider/'.$item->id.'/edit') !!}">Editar</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>
			@else
				<div class="alert alert-primary">Nenhuma foto de slider encontrada na base de dados.</div>
			@endif
		</div>
	</div>
@endsection