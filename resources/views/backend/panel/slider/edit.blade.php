@extends('layouts.backend')

@section('title-page', 'Slider Principal')

@section('contents')
	<div class="row">
		<div class="col-lg-12">
			@if(session()->has('alert-success'))
				<div class="alert alert-success">{!! session()->get('alert-success') !!}</div>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{!! $error !!}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>

					<h2 class="panel-title">Editar Slider</h2>
				</header>
				<div class="panel-body">
					{!! Form::open(['id' => 'form-slider', 'class' => 'form-horizontal form-bordered', 'files' => true, 'url' => 'admin/slider/'.$galeria->id]) !!}
						{{ method_field('PUT') }}
						<div class="form-group">
							<label class="col-md-3 control-label">Nome da Foto</label>
							<div class="col-md-6">
								{!! Form::text('titulo', $galeria->titulo, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Foto</label>
							<div class="col-md-6">
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input">
											<i class="fa fa-file fileupload-exists"></i>
											<span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-default btn-file">
											<span class="fileupload-exists">Trocar</span>
											<span class="fileupload-new">Selecione um Arquivo</span>
											<input type="file" name="foto" id="foto" />
										</span>
										<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remover</a>
										<div>A imagem precisa ter <strong>1920px</strong> de largura por <strong>923px</strong> de altura.</div>
									</div>
								</div>
							</div>
						</div>

						@if($galeria->foto)
							<div class="form-group">
								<label class="col-md-3 control-label">Foto Atual</label>
								<div class="col-md-6">
									{!! Html::image('img/sliders/'.$galeria->foto, $galeria->nome, ['width' => '200px']) !!}
								</div>
							</div>
						@endif

						<div class="form-group">
							<div class="col-xs-12">
								{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
								<a href="{!! url('admin/slider') !!}" class="btn btn-danger">Cancelar</a>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</section>
		</div>
	</div>
@endsection