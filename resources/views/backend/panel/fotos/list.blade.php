@extends('layouts.backend')

@section('title-page', 'Galeria de Fotos')

@section('contents')
	<!-- start: page -->
	<section class="content-with-menu content-with-menu-has-toolbar media-gallery">
		<div class="content-with-menu-container">
			<div class="inner-menu-toggle">
				<a href="#" class="inner-menu-expand" data-open="inner-menu">
					Opções <i class="fa fa-chevron-right"></i>
				</a>
			</div>
			
			<menu id="content-menu" class="inner-menu" role="menu">
				<div class="nano">
					<div class="nano-content">

						<div class="inner-menu-toggle-inside">
							<a href="#" class="inner-menu-collapse">
								<i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> Esconder
							</a>
							<a href="#" class="inner-menu-expand" data-open="inner-menu">
								Opções <i class="fa fa-chevron-down"></i>
							</a>
						</div>

						<div class="inner-menu-content">
							{!! Form::open(['id' => 'form-upload-fotos', 'files' => true, 'url' => 'admin/fotos']) !!}
								<a class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md" id="do-upload">
									<i class="fa fa-upload mr-xs"></i>
									Carregar Fotos
								</a>

								<button type="submit" class="btn btn-block btn-success btn-md pt-sm pb-sm text-md">
									<i class="fa fa-save mr-xs"></i>
									Salvar
								</button>

								<div id="count_files"></div>

								<input type="file" name="fotos[]" style="display: none" id="fotos" multiple>
								<hr class="separator" />

								<div class="message">
									Você pode carregar apenas <b>3</b> imagens de uma vez.
								</div>

								@if ($errors->any())
									<br>
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <li>{!! $error !!}</li>
								            @endforeach
								        </ul>
								    </div>
								@endif
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</menu>
			<div class="inner-body mg-main">
				<div class="inner-toolbar clearfix">
					<ul>
						<li>
							<a href="#" id="mgSelectAll"><i class="fa fa-check-square"></i> <span data-all-text="Selecionar Tudo" data-none-text="Remover Seleção">Selecionar Tudo</span></a>
						</li>
						<li>
							<a href="#" id="deleteFotos"><i class="fa fa-trash-o"></i> Deletar</a>
						</li>
					</ul>
				</div>
				<div class="row mg-files">
					@forelse($fotos as $item)
						<div class="col-sm-6 col-md-4 col-lg-3 o-foto">
							<div class="thumbnail">
								<div class="thumb-preview">
									<a class="thumb-image" href="{!! url('img/projetos/bigger/'.$item->foto) !!}">
										{!! Html::image('img/projetos/thumb/'.$item->foto, $item->nome, ['class' => 'img-responsive']) !!}
									</a>
									<div class="mg-thumb-options">
										<div class="mg-zoom"><i class="fa fa-search"></i></div>
										<div class="mg-toolbar">
											<div class="mg-option checkbox-custom checkbox-inline">
												<input type="checkbox" name="fotos-projetos" class="fotos-projetos" id="file-{!! $item->id !!}" value="1">
												<label for="file-{!! $item->id !!}">SELECIONAR</label>
											</div>
											<div class="mg-group pull-right">
												<button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
													<i class="fa fa-caret-up"></i>
												</button>
												<ul class="dropdown-menu">
													<li><a href="#modalEdit" class="modal-basic edit-foto" id="foto-{!! $item->id !!}"><i class="fa fa-edit"></i> Editar</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<h5 class="mg-title text-semibold">{!! $item->nome !!}</h5>
							</div>
						</div>
					@empty
						<div class="alert alert-primary">
							Você ainda não tem nenhuma foto. Clique no botão <strong>CARREGAR FOTOS</strong> ao lado esquerdo para começar.
						</div>
					@endforelse
				</div>
			</div>
		</div>
	</section>
	<!-- end: page -->

	<div id="modalEdit" class="modal-block modal-block-primary mfp-hide">
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Editar Projeto</h2>
			</header>
			<div class="panel-body">
				{!! Form::open(['id' => 'form-edit', 'autocomplete' => 'off', 'url' => 'admin/fotos/']) !!}
					<div class="form-group mt-lg">
						<label class="col-sm-3 control-label">Nome</label>
						<div class="col-sm-9">
							<input type="hidden" id="idfoto">
							<input type="text" name="nome" class="form-control" placeholder="Digite o Nome do Projeto" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Descrição</label>
						<div class="col-sm-9">
							<textarea rows="5" class="form-control" name="descricao" placeholder="Digite os detalhes do projeto"></textarea>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-md-12 text-right">
						<button class="btn btn-primary modal-confirm" id="saveDetail">Salvar</button>
						<button class="btn btn-default modal-dismiss">Cancelar</button>
					</div>
				</div>
			</footer>
		</section>
	</div>
@endsection