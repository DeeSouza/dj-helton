@extends('layouts.backend')

@section('title-page', 'Depoimentos')

@section('contents')
	<div class="row">
		<div class="col-lg-12">

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{!! $error !!}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>

					<h2 class="panel-title">Adicionar Depoimento</h2>
				</header>
				<div class="panel-body">
					{!! Form::open(['id' => 'form-depoimento', 'class' => 'form-horizontal form-bordered', 'files' => true, 'url' => 'admin/depoimentos']) !!}
						<div class="form-group">
							<label class="col-md-3 control-label" for="nome">Nome</label>
							<div class="col-md-6">
								{!! Form::text('nome', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">Sexo</label>
							<div class="col-md-6">
								{!! Form::select('sexo', ['F' => 'Feminino', 'M' => 'Masculino'], null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">Cargo</label>
							<div class="col-md-6">
								{!! Form::text('cargo', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">Depoimento</label>
							<div class="col-md-6">
								{!! Form::textarea('depoimento', null, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Foto</label>
							<div class="col-md-6">
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input">
											<i class="fa fa-file fileupload-exists"></i>
											<span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-default btn-file">
											<span class="fileupload-exists">Trocar</span>
											<span class="fileupload-new">Selecione um Arquivo</span>
											<input type="file" name="foto" id="foto" />
										</span>
										<div>A imagem precisa ter <strong>500px</strong> de largura por <strong>500px</strong> de altura.</div>
										<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remover</a>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-xs-12">
								{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
								<a href="{!! url('admin/depoimentos') !!}" class="btn btn-danger">Cancelar</a>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</section>
		</div>
	</div>
@endsection