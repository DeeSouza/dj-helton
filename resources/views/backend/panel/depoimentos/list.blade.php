@extends('layouts.backend')

@section('title-page', 'Depoimentos')

@section('contents')
	<div class="row">
		<div class="col-md-12">
			<a href="{!! url('admin/depoimentos/create') !!}" class="btn btn-primary">ADICIONAR DEPOIMENTO</a>
			<br /><br />

			@if(session()->has('alert-success'))
				<div class="alert alert-success">{!! session()->get('alert-success') !!}</div>
			@endif

			@if($depoimentos->count() > 0)
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
						</div>

						<h2 class="panel-title">Lista de Depoimentos</h2>
					</header>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover mb-none table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Nome</th>
										<th>Cargo</th>
										<th>Criado Em</th>
										<th>Opções</th>
									</tr>
								</thead>
								<tbody>
									@foreach($depoimentos as $item)
										<tr id="row-{!! $item->id !!}">
											<td>{!! $item->id !!}</td>
											<td>{!! $item->nome !!}</td>
											<td>{!! ($item->cargo) ?? 'Sem Cargo' !!}</td>
											<td>{!! $item->created_at->format('d/m/Y') !!}</td>
											<td>
												<a href="{!! url('admin/depoimentos/'.$item->id.'/edit') !!}">Editar</a> | 
												<a href="#modalCenterIcon" class="modal-basic delete-depoimento" id="depoimento-{!! $item->id !!}">Deletar</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>
			@else
				<div class="alert alert-primary">Nenhum depoimento encontrado na base de dados.</div>
			@endif
		</div>
	</div>

	<div id="modalCenterIcon" class="modal-block modal-block-primary mfp-hide">
		<section class="panel">
			<div class="panel-body text-center">
				<div class="modal-wrapper">
					<div class="modal-icon center">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="modal-text">
						<h4>Você tem certeza?</h4>
						<p>Tem certeza que deseja apagar esse depoimento?</p>

						<input type="hidden" id="iddepoimento">
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-md-12 text-right">
						<button class="btn btn-primary modal-confirm confirm-delete-depoimento">Confirmar</button>
						<button class="btn btn-default modal-dismiss">Cancelar</button>
					</div>
				</div>
			</footer>
		</section>
	</div>
@endsection