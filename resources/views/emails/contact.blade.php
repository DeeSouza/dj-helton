@component('mail::message')
# Dados do Usuário
---

**Nome:** {{ $data['nome'] }}  
**E-mail:** {{ $data['email'] }}  
**Telefone:** {{ $data['telefone'] }}  
**Data do Evento:** {{ $data['data_evento'] }}  
**Local do Evento:** {{ $data['local_evento'] }}  
**Tipo do Evento:** {{ $data['tipo_evento'] }}  

**Mensagem do Usuário:**

{{ $data['informacoes'] }}

Obrigado, <br>
Sistema de E-mails Helton Borges Eventos
@endcomponent