<nav id="menu" class="nav-main" role="navigation">
	<ul class="nav nav-main">
		<li class="nav-active">
			<a href="{!! URL::to('admin/panel') !!}">
				<i class="fa fa-home" aria-hidden="true"></i>
				<span>Página Principal</span>
			</a>
		</li>
		<li>
			<a href="{!! URL::to('admin/depoimentos') !!}">
				<i class="fa fa-star" aria-hidden="true"></i>
				<span>Depoimentos</span>
			</a>
		</li>
		<li>
			<a href="{!! URL::to('admin/fotos') !!}">
				<i class="fa fa-camera-retro" aria-hidden="true"></i>
				<span>Fotos</span>
			</a>
		</li>
		<li>
			<a href="{!! URL::to('admin/slider') !!}">
				<i class="fa fa-image" aria-hidden="true"></i>
				<span>Slider Principal</span>
			</a>
		</li>
	</ul>
</nav>