<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="Limonada Brasil">
	<meta name="description" content="Empresa prestadora de serviços para festas e eventos. DJ | SOM | LUZ | TELÃO | TVS | CONSULTORIA. Trabalhos com equipamentos novos e modernos e contamos com uma equipe treinada e preparada para garantir que a sua festa seja um sucesso.">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="keywords" content="helton borges eventos, eventos, casamentos, aniversários, 15 anos, corporativo, dj, som, luz, telão, tvs, festas">
	<title>Helton Borges Eventos - @yield('title', 'Página Inicial')</title>

	<link rel="apple-touch-icon" sizes="57x57" href="{!! URL::to('/apple-icon-57x57.png') !!}">
	<link rel="apple-touch-icon" sizes="60x60" href="{!! URL::to('/apple-icon-60x60.png') !!}">
	<link rel="apple-touch-icon" sizes="72x72" href="{!! URL::to('/apple-icon-72x72.png') !!}">
	<link rel="apple-touch-icon" sizes="76x76" href="{!! URL::to('/apple-icon-76x76.png') !!}">
	<link rel="apple-touch-icon" sizes="114x114" href="{!! URL::to('/apple-icon-114x114.png') !!}">
	<link rel="apple-touch-icon" sizes="120x120" href="{!! URL::to('/apple-icon-120x120.png') !!}">
	<link rel="apple-touch-icon" sizes="144x144" href="{!! URL::to('/apple-icon-144x144.png') !!}">
	<link rel="apple-touch-icon" sizes="152x152" href="{!! URL::to('/apple-icon-152x152.png') !!}">
	<link rel="apple-touch-icon" sizes="180x180" href="{!! URL::to('/apple-icon-180x180.png') !!}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{!! URL::to('/android-icon-192x192.png') !!}">
	<link rel="icon" type="image/png" sizes="32x32" href="{!! URL::to('/favicon-32x32.png') !!}">
	<link rel="icon" type="image/png" sizes="96x96" href="{!! URL::to('/favicon-96x96.png') !!}">
	<link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('/favicon-16x16.png') !!}">
	<link rel="manifest" href="{!! URL::to('/manifest.json') !!}">
	<meta name="msapplication-TileColor" content="#f18600">
	<meta name="msapplication-TileImage" content="{!! URL::to('/ms-icon-144x144.png') !!}">
	<meta name="theme-color" content="#f18600">

	{!! Html::style('css/app.css') !!}
	{!! Html::style('css/owl.carousel.min.css') !!}
	{!! Html::style('css/owl.theme.default.min.css') !!}
	{!! Html::style('js/fancybox/dist/jquery.fancybox.min.css') !!}
	{!! Html::style('css/animate.css') !!}
	{!! Html::style('css/all.min.css') !!}
</head>
<body data-base-url="{!! URL::to('/') !!}">

	<header>
		<a href="{!! URL::to('/') !!}" class="mobile">
			{!! Html::image('img/helton-borges-eventos-logo-text.png', 'Helton Borges Eventos') !!}
		</a>

		<a href="{!! URL::to('/') !!}" class="full">
			{!! Html::image('img/helton-borges-eventos-logo.png', 'Helton Borges Eventos') !!}
		</a>
	</header>

	<button type="button" id="open-menu">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</button>

	<nav>
		<ul>
			<li><a href="#quem-somos">QUEM SOMOS</a></li>
			<li><a href="#services">SERVIÇOS</a></li>
			<li><a href="#depoimentos">DEPOIMENTOS</a></li>
			<li><a href="#fale-conosco">CONTATO</a></li>
			<li></li>
		</ul>
	</nav>

	<main>
		<section id="sliders">
			<div class="owl-sliders owl-carousel">
				@foreach($sliders as $item)
					<div class="item">{!! Html::image('img/sliders/'.$item->foto, 'Helton Borges Eventos') !!}</div>
				@endforeach
			</div>

			<div class="slider-music">
				{!! Html::image('img/slider.png', 'Slider Music') !!}
			</div>
		</section>
		
		@yield('content')
	</main>

	<footer>
		<div class="redes-sociais">
			<a href="http://www.facebook.com/heltonborgeseventos">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3 8h-1.35c-.538 0-.65.221-.65.778v1.222h2l-.209 2h-1.791v7h-3v-7h-2v-2h2v-2.308c0-1.769.931-2.692 3.029-2.692h1.971v3z"/></svg>
			</a>

			<a href="http://www.instagram.com/heltonborgeseventos">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M17.833 6.595v1.476c0 .237-.193.429-.435.429h-1.465c-.238 0-.434-.192-.434-.429v-1.476c0-.237.195-.428.434-.428h1.465c.242 0 .435.191.435.428zm-5.833 7.498c1.121 0 2.028-.908 2.028-2.029s-.907-2.029-2.028-2.029-2.028.908-2.028 2.029.907 2.029 2.028 2.029zm12-2.093c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12zm-5-1.75h-3.953c.316.533.508 1.149.508 1.813 0 1.968-1.596 3.564-3.563 3.564-1.969 0-3.564-1.596-3.564-3.564 0-.665.191-1.281.509-1.813h-3.937v5.996c0 1.521 1.27 2.754 2.791 2.754h8.454c1.521 0 2.755-1.233 2.755-2.754v-5.996zm-7.009 4.559c1.515 0 2.745-1.232 2.745-2.746 0-.822-.364-1.56-.937-2.063-.202-.177-.429-.324-.677-.437-.346-.157-.729-.245-1.132-.245-.405 0-.788.088-1.133.245-.246.112-.474.26-.675.437-.574.503-.938 1.242-.938 2.063.001 1.514 1.234 2.746 2.747 2.746zm7.009-7.055c0-1.521-1.234-2.754-2.755-2.754h-7.162v2.917h-.583v-2.917h-.583v2.917h-.584v-2.872c-.202.033-.397.083-.583.157v2.715h-.583v-2.393c-.702.5-1.167 1.31-1.167 2.23v1.913h4.359c.681-.748 1.633-1.167 2.632-1.167 1.004 0 1.954.422 2.631 1.167h4.378v-1.913z"/></svg>
			</a>
			<span>heltonborgeseventos</span>
		</div>

		<div class="whatsapp">
			<span><strong>WhatsApp:</strong> </span> <span class="fone"><a href="#">19 99112-1506</a></span>
		</div>

		<div class="whatsapp">
			<span><strong>Contato:</strong> </span> <span class="email"><a href="mailto:helton@heltonborgeseventos.com.br">helton@heltonborgeseventos.com.br</a></span>
		</div>

		<div class="local">
			<address>Santa Bárbara D'Oeste | SP</address>
		</div>

		<div class="copyright">
			Copyright &copy; 2018 HELTON BORGES EVENTOS. Todos os direitos reservados. | <a href="http://www.limonadabrasil.com.br">Limonada Brasil</a>
		</div>
	</footer>

	@section('js')
		{!! Html::script('js/jquery.min.js') !!}
		{!! Html::script('js/owl.carousel.min.js') !!}
		{!! Html::script('js/jquery.mask.min.js') !!}
		{!! Html::script('js/jquery.validate.min.js') !!}
		{!! Html::script('js/fancybox/dist/jquery.fancybox.min.js') !!}
		{!! Html::script('js/app.js') !!}
	@show

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-84864525-4"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-84864525-4');
	</script>
	
</body>
</html>