<!doctype html>
<html class="fixed {!! Request::is('admin/fotos') ? 'sidebar-left-collapsed' : '' !!}">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Sistema Administrativo do Site - DJ Helton</title>

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		{!! Html::style("assets/vendor/bootstrap/css/bootstrap.css") !!}
		{!! Html::style("assets/vendor/font-awesome/css/font-awesome.css") !!}
		{!! Html::style("assets/vendor/magnific-popup/magnific-popup.css") !!}
		{!! Html::style("assets/vendor/bootstrap-datepicker/css/datepicker3.css") !!}

		<!-- Specific Page Vendor CSS -->
		{!! Html::style("assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css") !!}
		{!! Html::style("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css") !!}
		{!! Html::style("assets/vendor/morris/morris.css") !!}
		{!! Html::style("assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!}
		{!! Html::style("assets/vendor/isotope/jquery.isotope.css") !!}
		{!! Html::style("assets/vendor/pnotify/pnotify.custom.css") !!}

		<!-- Theme CSS -->
		{!! Html::style("assets/stylesheets/theme.css") !!}

		<!-- Skin CSS -->
		{!! Html::style("assets/stylesheets/skins/default.css") !!}

		<!-- Theme Custom CSS -->
		{!! Html::style("assets/stylesheets/theme-custom.css") !!}

		<!-- Head Libs -->
		{!! Html::script("assets/vendor/modernizr/modernizr.js") !!}

	</head>
	<body data-base="{!! URL::to('/') !!}">
		<section class="body">
			<!-- start: header -->
			@include('layouts.header')
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							MENU
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							@include('layouts.menu')
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>@yield('title-page')</h2>
					</header>

					@yield('contents')
				</section>
			</div>
		</section>

		<!-- Vendor -->
		{!! Html::script("assets/vendor/jquery/jquery.js") !!}
		{!! Html::script("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") !!}
		{!! Html::script("assets/vendor/bootstrap/js/bootstrap.js") !!}
		{!! Html::script("assets/vendor/nanoscroller/nanoscroller.js") !!}
		{!! Html::script("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js") !!}
		{!! Html::script("assets/vendor/magnific-popup/magnific-popup.js") !!}
		{!! Html::script("assets/vendor/jquery-placeholder/jquery.placeholder.js") !!}

		{!! Html::script("assets/vendor/pnotify/pnotify.custom.js") !!}

		<!-- Specific Page Vendor -->
		{!! Html::script("assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js") !!}
		{!! Html::script("assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js") !!}
		{!! Html::script("assets/vendor/jquery-appear/jquery.appear.js") !!}
		{!! Html::script("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js") !!}
		{!! Html::script("assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") !!}
		
		<!-- Theme Base, Components and Settings -->
		{!! Html::script("assets/javascripts/theme.js") !!}
		
		<!-- Theme Custom -->
		{!! Html::script("assets/javascripts/theme.custom.js") !!}
		
		<!-- Theme Initialization Files -->
		{!! Html::script("assets/javascripts/theme.init.js") !!}

		{!! Html::script("assets/vendor/pnotify/pnotify.custom.js") !!}

		<!-- Examples -->
		{!! Html::script("assets/javascripts/ui-elements/examples.modals.js") !!}
		{!! Html::script("assets/javascripts/pages/examples.mediagallery.js") !!}
	</body>
</html>