<header class="header">
	<div class="logo-container">
		<a href="{!! URL::to('admin/panel') !!}" class="logo">
			{!! Html::image("img/helton-borges-eventos-logo-text.png", "DJ Helton", ["height" => "25"]) !!}
		</a>
		<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="header-right">

		<span class="separator"></span>

		<div id="userbox" class="userbox">
			<a href="#" data-toggle="dropdown">
				<figure class="profile-picture">
					{!! Html::image("assets/images/!logged-user.jpg", "Admin", ["class" => "img-circle", "data-lock-picture" => "assets/images/!logged-user.jpg"]) !!}
				</figure>
				<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@JSOFT.com">
					<span class="name">Admin</span>
					<span class="role">Administrador</span>
				</div>

				<i class="fa custom-caret"></i>
			</a>
			
			<div class="dropdown-menu">
				<ul class="list-unstyled">
					<li class="divider"></li>
					<li>
						<a role="menuitem" tabindex="-1" href="{!! URL::to('auth/logout') !!}"><i class="fa fa-power-off"></i> Sair</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>