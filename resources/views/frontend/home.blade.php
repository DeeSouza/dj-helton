@extends('layouts.master')

@section('content')
	<section id="quem-somos">
		<div class="wrapper-header">
			<div class="image-logo">
				{!! Html::image('img/helton-borges-eventos-logo-text.png', 'Helton Borges Eventos') !!}
			</div>

			<div class="description">
				<h6>Equipe especializada na realização de</h6>
				<h5>Festas de 15 anos, Casamentos, Aniversários, Eventos Corporativos, Projetos Personalizados</h5>
			</div>
		</div>

		<div class="wrapper-sliders-eventos">
			{!! Html::image('img/slider-blue.png', 'Slider Blue', ['class' => 'img-slider-blue']) !!}

			<div class="owl-carousel owl-sliders-eventos">
				<div class="item">
					{!! Html::image('img/sliders-eventos/helton-borges-eventos-15-anos.jpg', '') !!}
					<div class="overlay">
						<h2>15 anos</h2>
					</div>
				</div>
				<div class="item">
					{!! Html::image('img/sliders-eventos/helton-borges-eventos-aniversarios.jpg', '') !!}
					<div class="overlay">
						<h2>Aniversários</h2>
					</div>
				</div>
				<div class="item">
					{!! Html::image('img/sliders-eventos/helton-borges-eventos-casamentos.jpg', '') !!}
					<div class="overlay">
						<h2>Casamentos</h2>
					</div>
				</div>
				<div class="item">
					{!! Html::image('img/sliders-eventos/helton-borges-eventos-corporativo.jpg', '') !!}
					<div class="overlay">
						<h2>Corporativo</h2>
					</div>
				</div>
				<div class="item">
					{!! Html::image('img/sliders-eventos/helton-borges-eventos-pessoais.jpg', '') !!}
					<div class="overlay">
						<h2>Projetos</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="services">
		<h2>SERVIÇOS</h2>

		<div class="owl-slider-services owl-carousel">
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-som.png', 'Helton Borges Eventos - Som') !!}</div>
					<div class="title">SOM</div>
				</div>
			</div>
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-luz.png', 'Helton Borges Eventos - Luz') !!}</div>
					<div class="title">LUZ</div>
				</div>
			</div>
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-dj.png', 'Helton Borges Eventos - DJ') !!}</div>
					<div class="title">DJ</div>
				</div>
			</div>
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-multimidia.png', 'Helton Borges Eventos - Multimídia') !!}</div>
					<div class="title">MULTIMÍDIA</div>
				</div>
			</div>
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-iluminacao.png', 'Helton Borges Eventos - Iluminação') !!}</div>
					<div class="title">ILUMINAÇÃO</div>
				</div>
			</div>
			<div class="item">
				<div class="wrapper-service">
					<div class="image">{!! Html::image('img/servicos/helton-borges-eventos-estruturas.png', 'Helton Borges Eventos - Estruturas') !!}</div>
					<div class="title">ESTRUTURAS</div>
				</div>
			</div>
		</div>
	</section>

	<section id="depoimentos">
		<h2>DEPOIMENTOS</h2>

		<div class="owl-slider-depoimentos owl-carousel">
			@forelse($depoimentos as $item)
				<div class="item">
					<div class="who">
						@if($item->foto)
							<div class="image">{!! Html::image('img/depoimentos/'.$item->foto, 'Depoimentos') !!}</div>
						@else 
							<div class="image">
								@if($item->sexo == 'F')
									{!! Html::image('img/depoimentos/avatar-f-'.rand(1,5).'.png', 'Depoimentos') !!}
								@else
									{!! Html::image('img/depoimentos/avatar-m-'.rand(1,4).'.png', 'Depoimentos') !!}
								@endif
							</div>
						@endif
						<div class="author">
							<div class="name">{!! $item->nome !!}</div>
							@if($item->cargo)
								<div class="job">{!! $item->cargo !!}</div>
							@endif
						</div>
					</div>
					<div class="description">{!! $item->depoimento !!}</div>
				</div>
			@empty

			@endforelse
		</div>
	</section>

	@if($fotos->count() > 0)
		<section id="projetos">
			<h2>O QUE TEMOS FEITO</h2>

			<div class="wrapper-projetos">
				@php
					$count = ($qtd = ceil($fotos->count() / 2)) < 3 ? 3 : $qtd
				@endphp

				@foreach($fotos->chunk($count) as $foto)
					<div class="owl-slider-projetos owl-carousel">
						@foreach($foto as $item)
							<div class="item">
								<a href="{!! URL::to('img/projetos/bigger/'.$item->foto) !!}" data-caption="{!! '<h4>'.$item->nome.'</h4><h6>'.$item->descricao.'</h6>' !!}" title="{!! $item->nome !!}" data-fancybox="gallery">
									<div class="image" style="background-image: url({!! URL::to('img/projetos/thumb/'.$item->foto); !!})"></div>
									<div class="title">{!! $item->nome !!}</div>
								</a>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
		</section>
	@endif

	<section id="fale-conosco">
		<h2>FALE CONOSCO</h2>

		<div class="wrapper-form">
			<div class="callback-send"></div>

			{!! Form::open(['id' => 'form-fale-conosco', 'autocomplete' => 'off']) !!}
				<div class="form-group">
					{!! Form::text('nome', null, ['class' => 'form-field', 'autocomplete' => 'off', 'placeholder' => 'Nome']) !!}
				</div>

				<div class="form-group form-inline-email">
					{!! Form::email('email', null, ['class' => 'form-field', 'autocomplete' => 'off', 'placeholder' => 'E-mail']) !!}
				</div>

				<div class="form-group form-inline-telefone">
					{!! Form::text('telefone', null, ['class' => 'form-field phone', 'autocomplete' => 'off', 'placeholder' => 'Telefone']) !!}
				</div>

				<div class="form-group form-inline-data">
					{!! Form::text('data_evento', null, ['class' => 'form-field dateevent', 'autocomplete' => 'off', 'placeholder' => 'Data do Evento']) !!}
				</div>

				<div class="form-group form-inline-local">
					{!! Form::text('local_evento', null, ['class' => 'form-field', 'autocomplete' => 'off', 'placeholder' => 'Local do Evento']) !!}
				</div>

				<div class="form-group">
					{!! Form::text('tipo_evento', null, ['class' => 'form-field', 'autocomplete' => 'off', 'placeholder' => 'Tipo do Evento']) !!}
				</div>

				<div class="form-group">
					{!! Form::textarea('informacoes', null, ['autocomplete' => 'off', 'placeholder' => 'Informações']) !!}
				</div>

				<div class="form-group align-right">
					{!! Form::submit('ENVIAR', ['class' => 'form-button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</section>

	<section id="helton-borges-eventos">
		{!! Html::image('img/helton-borges-eventos-logo.png', 'Helton Borges Eventos') !!}
	</section>
@endsection