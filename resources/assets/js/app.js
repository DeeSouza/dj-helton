var urlBase;

$(document).ready(function(){
	var widthDocument 	= $(window).width();
	urlBase 			= $('body').data('base-url');

	$.fancybox.defaults.hideScrollbar = false;

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('.owl-sliders').owlCarousel({
		items: 1,
		loop: true,
		navText: ['<i class="fas fa-arrow-alt-circle-left"></i>', '<i class="fas fa-arrow-alt-circle-right"></i>'],
		nav: true,
		dots: false
	});

	$('.owl-sliders-eventos').owlCarousel({
		items: 1,
		loop: true,
		navText: ['<i class="fas fa-arrow-alt-circle-left"></i>', '<i class="fas fa-arrow-alt-circle-right"></i>'],
		nav: true,
		autoplay: true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		autoplayTimeout: 5000,
		dots: false,
		responsive : {
			768: {
				nav: false
			}
		}
	});

	$('.owl-slider-services').owlCarousel({
		items: 1,
		loop: true,
		nav: false,
		autoplay: false,
		autoplayTimeout: 4500,
		margin: 0,
		dots: false,
		responsive : {
			568: {
				items: 2
			},
			768: {
				items: 3
			},
			1024: {
				items: 4
			},
			1280: {
				items: 5
			},
			1680: {
				items: 6,
				loop: false
			}
		}
	});

	$('.owl-slider-depoimentos').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: ['<i class="fas fa-arrow-alt-circle-left"></i>', '<i class="fas fa-arrow-alt-circle-right"></i>'],
		dots: false,
		margin: 10,
		responsive : {
			468: {
				navText: ['<img src="' + urlBase + '/img/icon-left.png" alt="Arrow Left" />', '<img src="' + urlBase + '/img/icon-right.png" alt="Arrow Right" />'],
			},
			568: {
				items: 2,
				center: true,
				nav: true
			},
			768: {
				items: 2,
				center: false,
				navText: ['<img src="' + urlBase + '/img/icon-left.png" alt="Arrow Left" />', '<img src="' + urlBase + '/img/icon-right.png" alt="Arrow Right" />'],
				nav: true
			},
			1024: {
				items: 3,
				center: false,
				nav: true,
				navText: ['<img src="' + urlBase + '/img/icon-left.png" alt="Arrow Left" />', '<img src="' + urlBase + '/img/icon-right.png" alt="Arrow Right" />'],
			},
			1280: {
				items: 3,
				margin: 70,
				navText: ['<img src="' + urlBase + '/img/icon-left.png" alt="Arrow Left" />', '<img src="' + urlBase + '/img/icon-right.png" alt="Arrow Right" />'],
			}
		}
	});

	$('.owl-slider-projetos').owlCarousel({
		items: 1,
		loop: false,
		nav: false,
		navText: ['<i class="fas fa-arrow-alt-circle-left"></i>', '<i class="fas fa-arrow-alt-circle-right"></i>'],
		dots: false,
		margin: 10,
		responsive : {
			568: {
				items: 2,
				center: true
			},
			1024: {
				items: 3,
				center: false
			},
			1280: {
				items: 3,
				margin: 42,
			}
		}
	});

	var els = $('nav, main, header, button#open-menu');
	$('#open-menu').on('click', function(){
		els.toggleClass('show-menu');
	});

	var positionInitial = 0;
	$(window).on('scroll', function(){
		var positionCurrent = $(this).scrollTop();

		if(positionInitial < positionCurrent && positionCurrent > 150){
			if(widthDocument < 1280){
				$('header, button#open-menu').addClass('hidden-scroll');
			}
			else{
				$('header').addClass('hidden-scroll');
			}
		}
		else if(positionInitial >= positionCurrent){
			$('header, button#open-menu').removeClass('hidden-scroll');
		}

		positionInitial = positionCurrent;

		// Overlay
		if(widthDocument >= 768)
			style.overlayStart();
	});

	var style = {
		overlayStart: function(){
			var overlay 	= $('.overlay');
			var width 		= $('.owl-sliders-eventos').width();
			var height 		= $('.owl-sliders-eventos').height();
			var newHeight 	= (width * 908) / 768;

			overlay.css({
				'width': width + 280 + 'px',
				'height': newHeight + 'px',
				'bottom': -newHeight + 'px'
			});
		}
	}

	if(widthDocument >= 768)
		style.overlayStart();

	var SPMaskBehavior = function (val) {
	  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		clearIfNotMatch: true,
		onKeyPress: function(val, e, field, options) {
	    	field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$('.dateevent').mask('00/00/0000', {
		clearIfNotMatch: true
	});

	$('.phone').mask(SPMaskBehavior, spOptions);

	$('#form-fale-conosco').validate({
		rules: {
			nome: "required",
			informacoes: "required",
			email: "required",
			telefone: "required"
		},
		messages: {
			nome: "Preencha este campo, por favor.",
			informacoes: "Preencha este campo, por favor.",
			email: "Preencha este campo, por favor.",
			telefone: "Preencha este campo, por favor."
		},
		submitHandler: function(form, event){
			event.preventDefault();

			var loadingBox 		= $('.callback-send')
			var contentLoading 	= '<div class="spinner">' +
										'<div class="rect1"></div>' +
									  	'<div class="rect2"></div>' +
									  	'<div class="rect3"></div>' +
									  	'<div class="rect4"></div>' +
									  	'<div class="rect5"></div>' +
									'</div>';
			$.ajax({
				url: urlBase + '/send-mail',
				type: "POST",
				data: $(form).serialize(),
				beforeSend: function(){
					loadingBox.addClass('show').html(contentLoading);
				},
				success: function(response){
					loadingBox.html(response.message);

					setTimeout(function(){
						loadingBox.html('').removeClass('show');
					}, 5000);
				}
			});
		}
	});

	if(widthDocument >= 768)
		style.overlayStart();

	$('nav ul li > a').on('click', function(event){
		event.preventDefault();

		var link = $(this).attr('href');

		$('html').animate({
			scrollTop: $(link).offset().top
		}, 500);

		setTimeout(function(){
			els.toggleClass('show-menu');
			if(widthDocument < 1280){
				$('header, button#open-menu').addClass('hidden-scroll');
			}
			else{
				$('header').addClass('hidden-scroll');
			}
		}, 1000)
	});
});